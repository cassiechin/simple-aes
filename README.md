# README #
* Cassie Chin
* cassiechin9793@gmail.com
* www.cassiechin.com

### What is this repository for? ###

* CSCE 4550
* Final Project
* Encryption System
* https://bitbucket.org/cassiechin/simple-aes

### Compile and Run Instructions ###
* compile with: gcc main.c
* run with: ./a.out

### Input File Conditions ###
* input.txt: Lines greater than 80 characters are ignored, all characters are uppercase
* key.txt: Only the first line is read, keys greater than 16 characters are ignored, all characters are uppercase

### Parts of the Project ###

* Preprocessing: Go through each character in a file. If it is not punctuation or whitespace, then keep it.
* Substitution: For each character add the next key character value onto it.
* Padding: The string length mod 16 are the number of characters in the last 4x4 block. Subtract that number from 16 to get the pad amount.
* Shift Rows: Go through the string line by line, and shift rows 0,1,2,3(repeat) times
* Parity Bit: For each character, count the number of 1's in the binary representation. If it is odd, then set the left most (MSB) bit to 1.
* Mix Columns: For each 4x4 block, compute the columns using the RGF matrix
