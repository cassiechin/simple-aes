/**
 * Cassie Chin (cassiechin9793@gmail.com)
 * CSCE 4550 Final Project
 * 
 * Encryption System
 * https://bitbucket.org/cassiechin/simple-aes
 * 
 ***********************************************************************
 * Plaintext input:
 * - stored in a file called input.txt
 * - each line must be 80 characters or less
 * 
 * Key input:
 * - stored in a file called key.txt
 * - can be a maximum of 16 characters
 * 
 * Program output:
 * - stored in a file called output.txt
 ***********************************************************************
 * How to run the program:
 * 
 * gcc main.c
 * ./a.out
 * 
 * (Note: Input must be stored in input.txt, key must be stored in key.txt)
 ***********************************************************************
 * Parts of Program:
 * 1) Preprocessing: Remove all whitespace and punctuation from input 
 * 2) Substitution: Run input through vigenere ciphere given the key
 * 3) Padding: Pad the result from step 2 so it fits in 4x4 blocks
 * 4) Shift Rows: For each block shift the rows 0,1,2,3 times respectively
 * 5) Parity Bit: Add a parity bit to each character that has an odd number of 1's
 * 6) Mix Columns: Multiply each column of each block by the RGF matrix
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#define INPUT_FILE "input.txt" // plaintext
#define LINE_LEN 80 // max line size
#define MAX_PLAINTEXT 5096 // max plaintext characters

#define KEY_FILE "key.txt" // key file
#define KEY_SIZE 16 // max key size

#define OUTPUT_FILE "output.txt"

#define TERMINAL_OUTPUT_ON

/**
 * Adds the k character to the c character and returns the result
 * @param c The character to encrypt
 * @param k Represents how much to shift the character
 * @return Return the new character
 */
char substitute (char c, char k) { return (((c-65) + (k-65)) % 26) + 65 ; }

/**
 * Multiply a and b and return the result.
 * @param a The number to multiply (any number)
 * @param b The number to multiply (either 2 or 3)
 * @return The result of the bitwise multiplication
 */
unsigned short rgfMul (unsigned short a, int b) {
	int overflow = 0; // says whether the msb is on or not
	if (a & (1 << 7)) overflow = 1; // the msb is on, so flag it as on
	unsigned short result = a << 1; // Bit shift to the left
	if (b == 3) result ^=  a; // Add itself if this is a "multiply by 3"
	if (overflow == 1) result ^= 283; // XOR result with 00011011 = 1+2+8+16 = 27, 100011011 = 283
	
	return result;
}

/**
 * Read plaintext in from a file. Remove all whitespace and punctuation.
 * @param inputFile The file name of the plaintext
 * @return All plaintext as a single string with the whitespace and punctuation removed.
 */
unsigned char *getPlaintextFromFile (char *inputFile) {
	FILE *f_input = fopen (INPUT_FILE, "r"); // input file	
    size_t len = 0; // getline parameter
    ssize_t read; // getline parameter
    unsigned char *plaintext = (unsigned char *) calloc (MAX_PLAINTEXT, sizeof(unsigned char)); // store all the plaintext
    int plaintextIndex = 0; // counter for plaintext
	char *line = NULL; // a line in the input file
	int i;
 	
    // Read in the plaintext    
    while ((read = getline(&line, &len, f_input)) != -1) {
        // Remove the new line at the end
        if (line[strlen(line)-1] == '\n') line[strlen(line)-1] = '\0';

		if (strlen(line) > LINE_LEN) continue; // ignore this line if it is greater than
		for (i=0; i<strlen(line); i++) { // read each line character by character
			if (ispunct(line[i]) || isspace(line[i])) continue; // ignore punctuation, spaces, and numbers
			plaintext[plaintextIndex++] = line[i]; // read in the plaintext character by character
		}
	}	
	return plaintext;
}

/**
 * Vigenere Cipher on plaintext given the filename of the key
 * @param plaintext The string of preprocessed plaintext
 * @param keyFile The name of the file containing the key
 * @return The enciphered string
 */
unsigned char *vignereSubstitutionFromPlaintext(unsigned char *plaintext, char* keyFile) {
	int i;
    unsigned char *ciphertext = (unsigned char *) calloc (MAX_PLAINTEXT, sizeof(unsigned char)); // store all the ciphertext
    int ciphertextIndex = 0; // counter for ciphertext
	char *line = NULL; // a line in the input file
    size_t len = 0; // getline parameter
    ssize_t read; // getline parameter
	FILE *f_key = fopen (KEY_FILE, "r"); // output file
	char *key = NULL; // the key from key.txt
    int keyIndex = 0; // counter for the keyIndex when doing the substitution
	int keysize; // number of characters in the key, program terminates if the key is too long

    // Read in the key
    if (!f_key) return NULL;
    read = getline(&key, &len, f_key);
    if (key[strlen(key)-1] == '\n') key[strlen(key)-1] = '\0';
    if (strlen(key) > KEY_SIZE || strlen(key) == 0) return NULL;
	keysize = strlen(key);
	
	for (i=0; i<strlen(plaintext); i++) {
		ciphertext[ciphertextIndex++] = substitute(plaintext[i], key[keyIndex]); // substitute directly and store the value
		keyIndex = (keyIndex + 1) % keysize; // Increment the key counter, wrap it around if it goes over		
	}
	return ciphertext;
}

/**
 * Pad a string so it has enough characters to fill a number of 4x4 blocks.
 * @param ciphertext The string to modify
 * @return The string with padded 'A' on the end
 */
unsigned char *padString (unsigned char *ciphertext) {
	// Compute the amount to pad
	int padAmount = 16 - (strlen(ciphertext) % 16); // Add enough padding to fill the last block

	// Store the return string here
	unsigned char *returnString = (unsigned char *) calloc (strlen(ciphertext) + padAmount + 1, sizeof(unsigned char));

	// Copy the current string into the new string
	strncpy (returnString, ciphertext, strlen(ciphertext));

	// Start padding at the end of this string
	int ciphertextIndex = strlen(ciphertext);
	
	// Pad the end of the string with 'A'	
	int i;
	for (i=0; i< padAmount; i++) returnString[ciphertextIndex++] = 'A';

	return returnString;
}

/**
 * For each 4x4 block shift the rows 0,1,2,3 times respectively
 * @param ciphertext The string to modify
 * @return The modified string
 */
unsigned char *shiftRows (unsigned char *ciphertext) {
	int startIndex = 0; // counter for shiftrows
	int shiftAmount = 0; // counter for shiftrows
	int i,j;
	
	for (startIndex=0, shiftAmount=0; startIndex<strlen(ciphertext); shiftAmount++, shiftAmount %= 4, startIndex += 4) {
		// Swap the first character to the end for the number of required times
		for (i=0; i<shiftAmount; i++) {
			// Shift the first character to the end
			for (j=startIndex; j<startIndex+4-1; j++) {
				int temp = ciphertext[j];
				ciphertext[j] = ciphertext[j+1];
				ciphertext[j+1] = temp;
			}
		}		
	}
	return ciphertext;
}

/**
 * If the number of 1's in the binary representation is ODD, then set the MSB to 1
 * http://www.geeksforgeeks.org/binary-representation-of-a-given-number/
 * http://stackoverflow.com/questions/47981/how-do-you-set-clear-and-toggle-a-single-bit-in-c-c
 * @param ciphertext The ciphertext to modify
 * @return The resulting string when a parity bit is added to the select characters.
 */
unsigned char *parityBit (unsigned char *ciphertext) {
	int i,j;
	    
	for (i=0; i<strlen(ciphertext); i++) {
		int bitsOn = 0; // The number of 1's
		unsigned char n = ciphertext[i]; // The character to check
		
		for (j=1<<7; j>0; j/=2) if (ciphertext[i] & j) bitsOn++; // Count the number of 1's in the binary representation
		if (bitsOn % 2 == 1) ciphertext[i] |= 1 << 7; // If the number of 1's is odd, then set the MSB to 1
	}	
	
	return ciphertext;
}

/**
 * Multiply the RGF matrix on each 4x4 block
 * @param ciphertext The string to modify
 * @return The modified string
 */
unsigned char *mixColumns (unsigned char *ciphertext) {
	int i,j;
	// Do one block at a time
	for (i=0; i<strlen(ciphertext); i += 16) {
		// Do one column at a time		
		for (j=0; j<4; j++) {
			// Save the column values
			unsigned char c0 = ciphertext[i+j+0];
			unsigned char c1 = ciphertext[i+j+4];
			unsigned char c2 = ciphertext[i+j+8];
			unsigned char c3 = ciphertext[i+j+12];		
			
			// Get the new column values
			// http://en.wikipedia.org/wiki/Rijndael_mix_columns
			ciphertext[i+j+0] = rgfMul(c0, 2) ^ rgfMul(c1, 3) ^ c2 ^ c3;
			ciphertext[i+j+4] = c0 ^ rgfMul(c1, 2) ^ rgfMul(c2, 3) ^ c3;
			ciphertext[i+j+8] = c0 ^ c1 ^ rgfMul(c2, 2) ^ rgfMul(c3, 3);
			ciphertext[i+j+12] = rgfMul(c0, 3) ^ c1 ^ c2 ^ rgfMul(c3, 2);
		}
	}	
	return ciphertext;
}

/**
 * Run all steps of the program in order and print out the results
 * @param inputFile The file name of the plaintext
 * @param outputFile The file name of the output
 * @param keyFile The file name of the key
 */
void simpleAES (char *inputFile, char *outputFile, char *keyFile) {
	FILE *f_out = fopen (outputFile, "w");
	if (!f_out) return;

	int i;
	unsigned char *plaintextStep = getPlaintextFromFile(inputFile);
	if (plaintextStep == NULL) return;
#ifdef TERMINAL_OUTPUT_ON
	printf("Preprocessing\n");
	printf("%s\n\n", plaintextStep);
#endif
	fprintf(f_out, "Preprocessing\n");
	fprintf(f_out, "%s\n\n", plaintextStep);
	
	unsigned char *substitutionStep = vignereSubstitutionFromPlaintext (plaintextStep, keyFile);
	if (substitutionStep == NULL) return;
#ifdef TERMINAL_OUTPUT_ON
	printf("Substitution\n");
	printf("%s\n\n", substitutionStep);
#endif
	fprintf(f_out, "Substitution\n");
	fprintf(f_out, "%s\n\n", substitutionStep);	
	
	unsigned char *paddingStep = padString (substitutionStep);
#ifdef TERMINAL_OUTPUT_ON
	printf("Padding\n");
	for (i=0; i<strlen(paddingStep); i++) {
		printf("%c", paddingStep[i]);
		if ((i+1) % 4 == 0) printf("\n");
		if ((i+1) % 16 == 0) printf("\n");
	}
#endif
	fprintf(f_out, "Padding\n");
	for (i=0; i<strlen(paddingStep); i++) {
		fprintf(f_out, "%c", paddingStep[i]);
		if ((i+1) % 4 == 0) fprintf(f_out, "\n");
		if ((i+1) % 16 == 0) fprintf(f_out, "\n");
	}
	
	unsigned char *shiftRowsStep = shiftRows (paddingStep);
#ifdef TERMINAL_OUTPUT_ON
	printf("Shift Rows\n");
	for (i=0; i<strlen(shiftRowsStep); i++) {
		printf("%c", shiftRowsStep[i]);
		if ((i+1) % 4 == 0) printf("\n");
		if ((i+1) % 16 == 0) printf("\n");
	}
#endif
	fprintf(f_out, "Shift Rows\n");
	for (i=0; i<strlen(shiftRowsStep); i++) {
		fprintf(f_out, "%c", shiftRowsStep[i]);
		if ((i+1) % 4 == 0) fprintf(f_out, "\n");
		if ((i+1) % 16 == 0) fprintf(f_out, "\n");
	}
	
	unsigned char *parityBitStep = parityBit (shiftRowsStep);
#ifdef TERMINAL_OUTPUT_ON
	printf("Parity Bit\n");
	for (i=0; i<strlen(parityBitStep); i++) {
		printf("%x ", parityBitStep[i]);
		if ((i+1) % 4 == 0) printf("\n");
		if ((i+1) % 16 == 0) printf("\n");
	}
	
#endif
	fprintf(f_out, "Parity Bit\n");
	for (i=0; i<strlen(parityBitStep); i++) {
		fprintf(f_out, "%x ", parityBitStep[i]);
		if ((i+1) % 4 == 0) fprintf(f_out, "\n");
		if ((i+1) % 16 == 0) fprintf(f_out, "\n");
	}	

	unsigned char *mixColumnsStep = mixColumns (parityBitStep);
#ifdef TERMINAL_OUTPUT_ON
	printf("Mix Columns\n");
	for (i=0; i<strlen(mixColumnsStep); i++) {
		printf("%x ", mixColumnsStep[i]);
		if ((i+1) % 4 == 0) printf("\n");
		if ((i+1) % 16 == 0) printf("\n");
	}
#endif	
	fprintf(f_out, "Mix Columns\n");
	for (i=0; i<strlen(mixColumnsStep); i++) {
		fprintf(f_out, "%x ", mixColumnsStep[i]);
		if ((i+1) % 4 == 0) fprintf(f_out, "\n");
		if ((i+1) % 16 == 0) fprintf(f_out, "\n");
	}	

	printf("View output in %s...\n", outputFile);
	fclose (f_out);
}

int main () {
	simpleAES(INPUT_FILE, OUTPUT_FILE, KEY_FILE);
	return 0;
}
